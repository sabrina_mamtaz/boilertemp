from django.db import models, transaction
from django.conf import settings
from django.shortcuts import get_object_or_404

from sqlalchemy import create_engine, MetaData
from sqlalchemy import inspect
from sqlalchemy.engine.url import make_url
from sqlalchemy.types import Date, Integer, Numeric,Boolean
from sqlalchemy.exc import OperationalError, ProgrammingError
from sqlalchemy_utils import create_database, drop_database, database_exists
from fernet_fields import EncryptedTextField


from .config import DBIndexStatuses






def limit_string_choices():
    return {'string_choices': Field.objects.filter(type = 'string')}



class Database(models.Model):
    name =  models.CharField(max_length=100)
    external_database = models.OneToOneField('ExternalDatabase', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_sqla_url(self):
        url = make_url(self.external_database.sqlalchemy_uri)
        url.password = self.external_database.password
        return url

    def get_sqla_engine(self):
        url = self.get_sqla_url()
        return create_engine(url, convert_unicode=True)


    def remove_database_on_delete(self):
        engine = self.get_sqla_engine()
        if database_exists(engine.url):
            drop_database(engine.url)




class ExternalDatabase(models.Model):
    sqlalchemy_uri = models.TextField()
    password = EncryptedTextField(blank=True)
    index_status = models.PositiveSmallIntegerField(choices=DBIndexStatuses.CHOICES,
                                                    default=DBIndexStatuses.IN_QUEUE)

    def __str__(self):
        return self.sqlalchemy_uri

    def index_tables(self):
        print('                                      inside index_tables')
        """Index the tables of external database"""
        engine = self.database.get_sqla_engine()
        tables_name = engine.table_names()
        print(tables_name)

        with transaction.atomic():
        #     # Remove existing tables
            self.database.tables.all().delete()

            for name in tables_name:
                Table.objects.create(name=name, database=self.database)





class Table(models.Model):
    name = models.CharField(max_length=100)
    database = models.ForeignKey(Database, on_delete=models.CASCADE,related_name='tables', default=None)
    has_permission = models.BooleanField(default=True)



    def __str__(self):
        return self.name

    def db(self):
        return self.db_id.value

    def get_inspector(self):
        return inspect(self.database.get_sqla_engine())



    def get_field_type(self, field_type):
        # TODO: Make it more optimized. currently its not a good way to determine actually. :(

        number_type_classes = [Numeric, Integer]
        date_type_classes = [Date]
       # bool_type_classes=[Boolean]

        for class_type in number_type_classes:
            if isinstance(field_type, class_type):
                return 'number'

        for class_type in date_type_classes:
            if isinstance(field_type, class_type):
                return "date"

        # for class_type in bool_type_classes:
        #     if isinstance(field_type, class_type):
        #         return "boolean"

        # If its not number, current way to support is to make it string
        return 'string'



    def get_fields_data(self):
        """Check the table and return the fields name and type in list of dictionary"""
        inspector = self.get_inspector()
        fields = inspector.get_columns(self.name)
        processed_fields =[]
        for field in fields:
            if not "autoincrement" in field or not field["autoincrement"]:
                processed_fields.append({"name": field["name"],
                                     "type": self.get_field_type(field["type"])})
        return processed_fields


    def index_fields(self):
        """Index the fields of the table."""
        print('                                      inside index_fields')
        fields_data = self.get_fields_data()
        fields_list = []

        for field_data in fields_data:
            field = Field(table=self, **field_data)
            fields_list.append(field)

        with transaction.atomic():
            # Remove existing fields
            self.fields.all().delete()
            # Bulk create the fields
            Field.objects.bulk_create(fields_list)

    def _get_relation_data(self):
        inspector = self.get_inspector()
        relations_data = inspector.get_foreign_keys(self.name)
        return relations_data


    def drop_table_from_db(self):
        engine = self.database.get_sqla_engine()

        if engine and engine.connect() and not self.database.external_database:
            meta = MetaData(bind=engine, reflect=True)
            if self.name in meta.tables:
                table = meta.tables[self.name]
                table.drop(checkfirst=True)




class Field(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE, related_name='fields', default=None)
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)


    def __str__(self):
        return self.name

    def tables(self):
        return self.table.value



class FieldOptions(models.Model):
    value = models.CharField(max_length=200)
    field = models.ForeignKey(Field, on_delete=models.CASCADE, related_name='options', default=None)


    def __str__(self):
        return self.value



















