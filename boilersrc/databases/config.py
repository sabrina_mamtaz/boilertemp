PASSWORD_MASK = "X" * 5



class DBIndexStatuses:
    IN_QUEUE = 0
    PROCESSING = 1
    PROCESSED = 2
    ERROR = 3

    CHOICES = (
        (IN_QUEUE, "In Queue"),
        (PROCESSING, "Processing"),
        (PROCESSED, "Processed"),
        (ERROR, "Error")
    )