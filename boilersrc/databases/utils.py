from copy import deepcopy

from .config import PASSWORD_MASK


def get_masked_sqla_url(sqlalchemy_url):
    copied_sqla_url = deepcopy(sqlalchemy_url)

    # If the sqlalchemy url has any password, mask it
    # Do not mask it if there are no password in the sqlalchemy url
    if copied_sqla_url.password and copied_sqla_url.password != PASSWORD_MASK:
        copied_sqla_url.password = PASSWORD_MASK

    return copied_sqla_url