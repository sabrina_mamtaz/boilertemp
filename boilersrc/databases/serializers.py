from rest_framework import serializers

from .models import Database,ExternalDatabase,Table,Field, FieldOptions



class ExternalDatabaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExternalDatabase
        exclude = ('password',)



class DatabaseSerializer(serializers.ModelSerializer):
    external_database = ExternalDatabaseSerializer()

    class Meta:
        model = Database
        fields = ('id', 'name', 'external_database')



class TableSerializer(serializers.ModelSerializer):
    database = DatabaseSerializer
    db_name = serializers.CharField(source='database.name', read_only=True)

    class Meta:
        model = Table
        fields = ('id','name', 'db_name','has_permission')


class FieldOptionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldOptions
        fields = ('id', 'value')


class FieldSerializer(serializers.ModelSerializer):
    table = TableSerializer
    options =  FieldOptionsSerializer(many=True)
    table_name = serializers.CharField(source='table.name', read_only=True)

    class Meta:
        model = Field
        fields = ('id','name', 'type','table_name','options')
















