from .config import DBIndexStatuses
from .models import ExternalDatabase
from boilersrc.celery import app



#logger = logging.getLogger(__name__)


@app.task
def index_external_database_task(external_db_id):
    external_db_queryset = ExternalDatabase.objects.filter(id=external_db_id)
    print('just checking in task')
    if external_db_queryset.exists():
        print('doing the task')
        external_db = external_db_queryset[0]

        # Change the `index_status` of the external database
        external_db_queryset.update(index_status=DBIndexStatuses.PROCESSING)

        # Index the tables
        external_db.index_tables()

        # # Index the external database into NLQ
        # database.nlq_handler.index_database()

        external_db_queryset.update(index_status=DBIndexStatuses.PROCESSED)
