from django.apps import AppConfig


class DatabasesConfig(AppConfig):
    name = 'databases'

    def ready(self):
        from .signals import (post_save_database,pre_save_database, create_fields_for_tables,
                              delete_database_information,drop_local_database,drop_table)