from django.contrib import admin

from .models import ExternalDatabase, Field, Database, Table


class DBAdmin(admin.ModelAdmin):
    list_display = ('name','external_database')

class TableAdmin(admin.ModelAdmin):
    list_display = ('name','database')

class FieldAdmin(admin.ModelAdmin):
    list_display = ('table','name','type')

class ExternalDatabaseAdmin(admin.ModelAdmin):
    list_display = ('sqlalchemy_uri', 'index_status')

admin.site.register(Database,DBAdmin)
admin.site.register(Table, TableAdmin)
admin.site.register(Field,FieldAdmin)
admin.site.register(ExternalDatabase,ExternalDatabaseAdmin)
