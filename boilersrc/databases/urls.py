from django.conf.urls import url
from .views import DatabaseListCreate, TablePermissionView, DatabaseListView, TablePermissionChangeView,FieldOptionDelete,\
    TableListView,ExtDatabaseCreateView,FieldListView,CsvUploadView, AddOptionToFields
urlpatterns = [
    url(r'^$', DatabaseListCreate.as_view(), name='database_test'),
    url(r'^tables$', TablePermissionView.as_view(), name='permission_view'),#depends on user
    url(r'^create/external_database$', ExtDatabaseCreateView.as_view(), name='ext_data_create_view'),

    url(r'^(?P<database_id>[0-9]+)$', DatabaseListView.as_view(), name='database_view'),
    url(r'^(?P<database_id>[0-9]+)/tables$',TableListView.as_view(), name='table_view' ),
    url(r'^tables/(?P<table_id>[0-9]+)/fields$',FieldListView.as_view(), name='field_view' ),
    url(r'^fields/(?P<field_id>[0-9]+)/options$', AddOptionToFields.as_view(), name='field_options'),
    url(r'^tables/(?P<table_id>[0-9]+)/permission$', TablePermissionChangeView.as_view(), name='permission_change_view'),
    url(r'^options/(?P<option_id>[0-9]+)$', FieldOptionDelete.as_view(), name='option_delete_view'),

    url(r'^upload_csv$', CsvUploadView.as_view(), name='upload_view')
]