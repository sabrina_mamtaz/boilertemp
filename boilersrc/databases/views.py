from django.db import transaction
from django.http import JsonResponse
from rest_framework import status
from rest_framework.generics import (ListCreateAPIView, ListAPIView, UpdateAPIView, DestroyAPIView)
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Database, ExternalDatabase, Table,Field, FieldOptions

from .serializers import DatabaseSerializer, TableSerializer,FieldSerializer, FieldOptionsSerializer

import ast




class DatabaseListCreate(ListAPIView):
    queryset = Database.objects.all()
    serializer_class = DatabaseSerializer


class DatabaseListView(ListAPIView):
    serializer_class = DatabaseSerializer
    lookup_field = 'id'
    lookup_url_kwarg = 'database_id'

    def get_queryset(self):
        database_id = self.kwargs.get(self.lookup_url_kwarg)
        print(database_id)
        return Database.objects.filter(id=database_id)

    def get(self, request, *args, **kwargs):
        database= self.get_queryset()
        db = database[0]
        return Response(self.serializer_class(db).data)





class TablePermissionView(ListAPIView):
    serializer_class = TableSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Table.objects.all()
        else:
            Table.objects.filter(has_permission=True)



class TableListView(ListAPIView):
    serializer_class = TableSerializer

    lookup_field = 'id'
    lookup_url_kwarg = 'database_id'

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Table.objects.filter(database_id = self.kwargs['database_id'])
        else:
            return Table.objects.filter(database_id=self.kwargs['database_id'],has_permission=True)


class TablePermissionChangeView(UpdateAPIView):
    serializer_class = TableSerializer
    queryset = Table.objects.all()


    lookup_field = 'id'
    lookup_url_kwarg = 'table_id'

    def get_queryset(self):
        table_id = self.kwargs.get(self.lookup_url_kwarg)
        print(table_id)
        return Table.objects.get(id=table_id)

    def partial_update(self, request, *args, **kwargs):
        table = self.get_queryset()
        table.has_permission = request.data.get("has_permission")
        table.save()

        return Response(self.serializer_class(table).data)





class FieldListView(ListAPIView):
    serializer_class = FieldSerializer

    lookup_field = 'id'
    lookup_url_kwarg = 'table_id'

    def get_queryset(self):
        return Field.objects.filter(table_id = self.kwargs['table_id'])





class ExtDatabaseCreateView(ListCreateAPIView):
    serializer_class = DatabaseSerializer

    def perform_create(self, serializer):
        with transaction.atomic():
            external = serializer.validated_data.pop('external_database', '')
            external_database = ExternalDatabase.objects.create(**external)
            serializer.save(external_database = external_database)



def insertStringCSV(name,data):
    fields = data[0]
    query = 'INSERT INTO ' + name + ' ('
    j = 0
    length = len(fields)
    for column in fields:
        query += (column)
        j += 1
        if j < length:
            query += ','
    query += ') VALUES '

    ar_len = len(data) - 1

    for i in range(1, ar_len):
        fields = data[i]
        query += '('
        j = 0
        length = len(fields)
        for column in fields:
            col_str = '\'' + column + '\''
            query += (col_str)
            j += 1
            if j < length:
                query += ','

        query += ')'
        if i + 1 < ar_len:
            query += ', '

    query += ';'

    return query


class CsvUploadView(APIView):
    def post(self, request):

        table_id = request.data.get('table_id')
        data = request.data.get('data')

        table = Table.objects.get(id = table_id)
        db = table.database
        table_name = table.name

        engine = db.get_sqla_engine()
        connection = engine.connect()


        query = insertStringCSV(table_name,data)
        print(query)


        connection.execute(query)

        connection.close()


        return JsonResponse({'status':'ok'},status = status.HTTP_200_OK)




class AddOptionToFields(APIView):
    serializer_class = FieldOptionsSerializer
    lookup_field = 'id'
    lookup_url_kwarg = 'field_id'


    def post(self, request, *args, **kwargs):


        field_id = self.kwargs['field_id']
        option = request.data.get('option')

        field = Field.objects.get(id = field_id)


        insert_option = FieldOptions.objects.create(value = option, field = field)
        insert_option.save()

        return Response(self.serializer_class(insert_option).data, status=status.HTTP_200_OK)



class FieldOptionDelete(DestroyAPIView):
    serializer_class = FieldOptionsSerializer
    queryset = FieldOptions.objects.all()

    lookup_field = 'id'
    lookup_url_kwarg = 'option_id'

    def perform_destroy(self, instance):
        option_id = self.kwargs['option_id']
        FieldOptions.objects.filter(id=option_id).delete()

        return JsonResponse({'status': 'ok'}, status=status.HTTP_200_OK)

















