# Generated by Django 2.1.3 on 2019-01-03 10:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('databases', '0007_auto_20190103_1045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Database',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('external_database', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='databases.ExternalDatabase')),
            ],
        ),
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('database', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='tables', to='databases.Database')),
            ],
        ),
        migrations.RemoveField(
            model_name='stringinputoptions',
            name='field',
        ),
        migrations.DeleteModel(
            name='StringInputOptions',
        ),
        migrations.AddField(
            model_name='field',
            name='table',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='fields', to='databases.Table'),
        ),
    ]
