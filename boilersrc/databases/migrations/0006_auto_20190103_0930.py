# Generated by Django 2.1.3 on 2019-01-03 09:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('databases', '0005_stringinputoptions'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='database',
            name='conn_string',
        ),
        migrations.RemoveField(
            model_name='database',
            name='password',
        ),
        migrations.AlterField(
            model_name='stringinputoptions',
            name='field',
            field=models.ForeignKey(limit_choices_to={'string_choices': ()}, on_delete=django.db.models.deletion.CASCADE, related_name='strings', to='databases.Field'),
        ),
    ]
