from django.db.models.signals import post_save, pre_save, pre_delete, post_delete
from django.dispatch import receiver
from sqlalchemy.engine.url import make_url

from .utils import get_masked_sqla_url
from .models import Database, Table, ExternalDatabase
from .config import PASSWORD_MASK, DBIndexStatuses
from .tasks import index_external_database_task


@receiver(pre_save, sender=ExternalDatabase)
def pre_save_database(sender, instance, **kwargs):
    sqlalchemy_url = make_url(instance.sqlalchemy_uri)

    if not instance.id:
        # no id, that means its a new object
        # A new object can have anything as password
        # Save the password in encrypted field
        instance.password = sqlalchemy_url.password or ""
    else:
        # Its a existing object.
        # It may have the mask password.
        # If the password is not masked password, that means the password has been changed
        # So save the new password in encrypted field
        if sqlalchemy_url.password != PASSWORD_MASK:
            instance.password = sqlalchemy_url.password

    # Masked the sqlalchemy uri
    instance.sqlalchemy_uri = str(get_masked_sqla_url(sqlalchemy_url))


@receiver(post_save, sender=Database)
def post_save_database(sender, instance, created, **kwargs):
    print('receiving signals from database')
    if instance.external_database_id:
        if instance.external_database.index_status == DBIndexStatuses.IN_QUEUE:
            print('             doing the work')
            index_external_database_task.delay(external_db_id=instance.external_database_id)



@receiver(pre_delete, sender=Database)
def drop_local_database(sender, instance, using, **kwargs):
    if not instance.external_database:
        instance.remove_database_on_delete()


@receiver(post_delete, sender=Database)
def delete_database_information(sender, instance, using, **kwargs):
    if instance.external_database:
        external_database = ExternalDatabase.objects.get(id=instance.external_database.id)
        external_database.delete()



@receiver(post_save, sender=Table)
def create_fields_for_tables(sender, instance, created, **kwargs):
    print('receiving signals from table')
    if created and instance.database.external_database:
        print('              doing the work')
        instance.index_fields()


@receiver(pre_delete, sender=Table)
def drop_table(sender, instance, using, **kwargs):
    instance.drop_table_from_db()