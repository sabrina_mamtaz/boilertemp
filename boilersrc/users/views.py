from django.shortcuts import render
from rest_framework.generics import (ListCreateAPIView, RetrieveAPIView)
from rest_framework.response import Response

from .models import User
from .serializers import UserSerializer
# Create your views here.


class UserListCreate(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(id = user.id)