from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin

from .models import User


class UserFieldAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'first_name', 'last_name', 'email', 'is_active', 'is_staff', 'is_superuser',
                                       )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'first_name', 'last_name', 'email'),
        }),
    )
    filter_horizontal = ('user_permissions',)

admin.site.register(User, UserFieldAdmin)